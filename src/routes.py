from run import api
from src.controllers import auth_controller as ac

api.add_resource(ac.UserRegistration, '/registration')
api.add_resource(ac.UserLogin, '/login')
api.add_resource(ac.UserLogoutAccess, '/logout/access')
api.add_resource(ac.UserLogoutRefresh, '/logout/refresh')
api.add_resource(ac.TokenRefresh, '/token/refresh')
api.add_resource(ac.AllUsers, '/users')
api.add_resource(ac.SecretResource, '/secret')
