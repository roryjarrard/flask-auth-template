from os import getenv

from dotenv import load_dotenv
from flask import Flask, jsonify
from flask_jwt_extended import JWTManager
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy

load_dotenv()

app = Flask(__name__)
api = Api(app)

db_user = getenv('DB_USER')
db_host = getenv('DB_HOST')
db_pass = getenv('DB_PASS')
db_name = getenv('DB_NAME')

app.config['PROPAGATE_EXCEPTIONS'] = getenv('PROPAGATE_EXCEPTIONS')
app.config['SQLALCHEMY_DATABASE_URI'] = f'mysql://{db_user}:{db_pass}@{db_host}/{db_name}'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = getenv('SQLALCHEMY_TRACK_MODIFICATIONS')

db = SQLAlchemy(app)


@app.before_first_request
def create_tables():
    db.create_all()


app.config['JWT_SECRET_KEY'] = getenv('JWT_SECRET_KEY')
app.config['JWT_BLACKLIST_ENABLED'] = getenv('JWT_BLACKLIST_ENABLED')
app.config['JWT_BLACKLIST_TOKEN_CHECKS'] = ['access', 'refresh']
jwt = JWTManager(app)

from src import routes
from src.models import RevokedTokenModel

@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    jti = decrypted_token['jti']
    return RevokedTokenModel.is_jti_blacklisted(jti)


@app.route('/')
def index():
    return jsonify({'message': 'hello'})
