# FLASK AUTHENTICATION TEMPLATE

Template for a project starter with authentication routes (registration, login, token refresh, etc.) utilizing JWT tokens

## Setup

1. Pull the project
    - `git clone git@bitbucket.org:roryjarrard/flask-auth-template.git`

2. Copy `.git.example` to `.git` and change values appropriately (database credentials, etc.)

3. Reinitiaze git `git init` or change remote to your project `git remote set-url origin git@...`

4. Run the server
    - `source .env && flask run`

